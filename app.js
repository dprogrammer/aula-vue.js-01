// Instrutor -> Lembrando que agora precisamos que os filtros sejam carregados antes da instrução "new Vue", para que o Vue já possa usa-los de cara.
Vue.filter('situacaoContasText', function(value){
    if (value === false) { // se colocar == então 0 == false dá true'
        return "Nenhuma conta cadastrada"
    } else if (value === 0) {
        return "Nenhuma conta a pagar"
    } else {
        return "Existem "+ value +" contas a serem pagas"
    }
})

var app = new Vue({
    el: "#app",
    data: {
        title: "Contas a pagar",
        teste: '',
        menus: [
            {id: 0, name: "Listar Contas"},
            {id: 1, name: "Criar Conta"}
        ],
        activedView: 1,
        formType: 'insert',
        bill: {
            date_due: '',
            name: '',
            value: 0,
            done: false
        },
        names: [
            'Conta de Luz',
            'Conta de Água',
            'Conta de Telefone',
            'Supermercado',
            'Cartão de Crédito',
            'Empréstimo',
            'Gasolina'
        ],
        bills: [
            {date_due: '20/08/2016', name: 'Conta de Luz', value: 101.87, done: true},
            {date_due: '21/08/2016', name: 'Conta de Água', value: 30.54, done: true},
            {date_due: '22/08/2016', name: 'Conta de Telefone', value: 50.01, done: true},
            {date_due: '23/08/2016', name: 'Supermercado', value: 287.32, done: true},
            {date_due: '24/08/2016', name: 'Cartão de Crédito', value: 326.08, done: true},
            {date_due: '25/08/2016', name: 'Empréstimo', value: 987.98, done: false},
            {date_due: '26/08/2016', name: 'Gasolina', value: 50.00, done: false}
        ]
    },
    computed: {
        statusOld: function () {
            // começo das aulas'
            var count = 0;

            for ( var i in this.bills ) {
               if (!this.bills[i].done ) {
                   count++;
                   console.log("valor i: " + i + " - valor count: " + count);
               } 
            }

            return !count ? "Nenhuma conta a pagar" : "Existem " + count + " a serem pagas";
        },
        status: function () {
            // novo status para o exercício
            var countPagos = 0;
            var countNaoPagos = 0;
            var statusTemp = false;

            for ( var i in this.bills ) {
               if (!this.bills[i].done ) {
                   countNaoPagos++;
                   console.log("valor i: " + i + " - valor count: " + countNaoPagos);
               } else {
                   countPagos++;
                   console.log("valor i: " + i + " - valor count: " + countPagos);
               }
            }

            if (countPagos > 0 && countNaoPagos == 0) {
                statusTemp = 0;
            } else if (countNaoPagos > 0) {
                statusTemp = countNaoPagos;
            }

            return statusTemp;
        }
    },
    methods: {
        showView: function ($event, id) {

            this.activedView = id;
            
            if (id = 1) {
                this.formType = 'insert';
            }
            
            this.teste = "OK!!!";

            console.log('link clicado showView: ' + id);
            console.log($event);
        },
        submit: function () {
            if (this.formType == 'insert') {
                this.bills.push(this.bill);
            } 
            
            this.bill = {
                date_due: '',
                name: '',
                value: 0,
                done: false
            };

            this.activedView = 0;
        },
        loadBill: function(bill) {
            this.bill = bill;
            this.activedView = 1;
            this.formType = 'update';
        },
        deleteBill: function(bill) {
            //http://stackoverflow.com/a/12582246
            var x = confirm("Deseja confirma a exclusão da conta " + bill.name + "?");
                
            if (x)
                this.bills.$remove(bill);
            
                //http://stackoverflow.com/a/35459981
        }
    }
});

// pode usar só um parâmetro'
app.$watch('teste', function(novoValor, velhoValor) {
    console.log("Velho valor: " + velhoValor + ", Novo Valor: " + novoValor);
})

Vue.filter('doneLabel', function(value){
    if (!value) {
        return "Não Paga"
    } else {
        return "Paga"
    }
})

